﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NZ.Railways.Models
{
    public class BookingDto
    {
        [Required(ErrorMessage = "* A valid departure city is required.")]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "* A valid departure city is required.")]
        public string StartStation { get; set; }

        [Required(ErrorMessage = "* A valid arrival city is required.")]
        [StringLength(5, MinimumLength = 1, ErrorMessage = "* A valid arrival city required.")]
        public string EndStation { get; set; }
    }
}